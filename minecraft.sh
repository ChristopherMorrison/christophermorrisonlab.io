#!/bin/bash
# Install minecraft server, from homelab project
set -ex
apt update
apt install -y curl openjdk-21-jre jq

# Setup user and service dir
getent passwd minecraft || useradd minecraft
mkdir -p /srv/minecraft/

# Get server.jar
curl -s https://launchermeta.mojang.com/mc/game/version_manifest.json |
    jq '[.versions[] | select(.type=="release")] | first | .url' |
    xargs curl -s |
    jq '.downloads.server.url' |
    xargs curl -o /srv/minecraft/server.jar

echo "eula=true" > /srv/minecraft/eula.txt
chown minecraft:minecraft -R /srv/minecraft

# Systemd unit
cat <<EOF > /lib/systemd/system/minecraft.service
[Unit]
Description=Minecraft Server
Wants=network-online.target
After=network-online.target

[Service]
# Ensure to set the correct user and working directory (installation directory of your server) here
User=minecraft
WorkingDirectory=/srv/minecraft

# You can customize the maximum amount of memory as well as the JVM flags here
ExecStart=/usr/bin/java -Xms1024M -Xmx1024M -jar server.jar --nogui

# Restart the server when it is stopped or crashed after 30 seconds
# Comment out RestartSec if you want to restart immediately
#Restart=always
#RestartSec=30

# Alternative: Restart the server only when it stops regularly
#Restart=on-success

# Do not remove this!
StandardInput=null

[Install]
WantedBy=multi-user.target 
EOF
cat /lib/systemd/system/minecraft.service
systemctl daemon-reload
systemctl enable --now minecraft.service
